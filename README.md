# Mirror Link signature sample project

Simple hello world project that demonstrates how to automate app signing for mirror link.

When building in DEBUG configuration, an DEVELOPER certificate is embedded in the app. The app is suitable to run on your test devices that have been declared in acms.carconnectivity.org

When building in RELEASE configuration, an ACMS certificate is embedded in the app. The app is suitable to be released on google play, and a certificate request can be issued from acms.carconnectivity.org


- in app/bin/certgen.py, set developer_id to your ACMS developer id
- in app/build.gradle, set your keystore, store-pass, key-pass, key-alias to your release keystore settings

```
    "--keystore", "<Path to release keystore>",
    "--store-pass", "<Keystore passphrase>",
    "--key-pass", "<Key passphrase, usually same as store passphrase>",
    "--key-alias", "<key alias>",
```


