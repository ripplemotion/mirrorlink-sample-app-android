#!/usr/bin/env python

import argparse
import os.path
import subprocess
import contextlib
import shutil

@contextlib.contextmanager
def chdir(path):
    """
    Usage:
    >>> with chdir(gitrepo_path):
    ...   subprocess.call('git status')
    """
    starting_directory = os.getcwd()
    try:
        os.chdir(path)
        yield
    finally:
        os.chdir(starting_directory)




def main():

    parser = argparse.ArgumentParser(description='Repackage apk for mirrorlink.')
    parser.add_argument('apk', metavar='apk', type=str, nargs='+',
                    help='apk to repackage')
    parser.add_argument('--xml', dest="cert_xml", metavar='xml', type=str,
                        help='certificat xml file')
    parser.add_argument('--build-tools', dest="build_tools", metavar='build_tools', type=str,
                    help='Android SDK build tools location')

    parser.add_argument('--keystore', dest="key_store", metavar='key_store', type=str, default='~/.android/debug.keystore',
                help='Keystore to use')

    parser.add_argument('--store-pass', dest="store_pass", metavar='store_pass', type=str, default='android',
                            help='Keystore to use')
    parser.add_argument('--key-pass', dest="key_pass", metavar='key_pass', type=str, default='android',
                                help='key pass to use')
    parser.add_argument('--key-alias', dest="key_alias", metavar='key_alias', type=str, default='androiddebugkey',
                                help='key alias to use')

    args = parser.parse_args()

    issuer = "CN=RippleMotion"
    developer_id = "471525401f5997c53f0e9e69daa7cc4f63b1f2d8"
    #

    build_tools = args.build_tools
    zipalign = os.path.join(build_tools, "zipalign")
    aapt = os.path.join(build_tools, "aapt")

    def resign(apk_path, keystore, storepass, keypass, key_alias):
        #unsign apk_path



        subprocess.check_output("zip -d %s META-INF/* || true" % apk_path, shell=True) #can fail if META-INF not in apk

        #resign with debug and our jarsigner
        subprocess.check_output("jarsigner -sigfile CERT -sigalg SHA1withRSA -digestalg SHA1 -keystore %s -storepass %s -keypass %s %s %s" % (keystore, storepass, keypass, apk_path, key_alias), shell=True)
        subprocess.check_output("jarsigner -verify %s" % apk_path, shell=True)
        #now apk is unaligned
        tmp_apk =  "%s.tmp" % apk_path
        shutil.copy(apk_path, tmp_apk)
        try:
            subprocess.check_output(zipalign + " -f 4 %s %s" % (tmp_apk, apk_path), shell=True)
            #check alignment
            subprocess.check_output(zipalign + " -c 4 %s" % apk_path, shell=True)
        finally:
            os.unlink(tmp_apk)

    cert_xml_path = args.cert_xml
    for apk_path in args.apk:
        assert os.path.exists(apk_path)
        assert os.path.exists(cert_xml_path)

        apk_abs_path = os.path.abspath(apk_path)

        build_dir = os.path.join(os.path.dirname(__file__) , '..', 'build')
        build_dir = os.path.abspath(build_dir)
        jar_path = os.path.join(os.path.dirname(__file__), 'certificategenerator-mac-64.jar')
        cert_path = os.path.join(build_dir, 'intermediates', 'mirrorlink', 'debug', 'assets', 'self-signed.ccc.crt')

        if not os.path.exists(os.path.dirname(cert_path)):
            os.makedirs(os.path.dirname(cert_path))

        if os.path.exists(cert_path):
            os.unlink(cert_path)

        backup_path = os.path.join(os.path.dirname(apk_path), os.path.splitext(os.path.basename(apk_path))[0] + '-premirrorlink.apk')
        shutil.copy(apk_path, backup_path) #make a copy
        resign(apk_path, keystore=args.key_store, storepass=args.store_pass, keypass=args.key_pass, key_alias=args.key_alias)
        #first call certgen to generate ccc cert
        cmd = "java -XstartOnFirstThread -jar %s generate-certificate %s %s %s %s %s" % (
            jar_path,
            apk_path,
            cert_xml_path,
            cert_path,
            issuer,
            developer_id)

        subprocess.check_output(cmd, shell=True)

        assert os.path.exists(cert_path)
        with chdir(os.path.join(build_dir, 'intermediates', 'mirrorlink', 'debug',)):
            subprocess.check_output(aapt + " a %s assets/self-signed.ccc.crt" % apk_abs_path, shell=True)
        #resign the apk
        resign(apk_path, keystore=args.key_store, storepass=args.store_pass, keypass=args.key_pass, key_alias=args.key_alias)




if __name__ == '__main__':
    main()
